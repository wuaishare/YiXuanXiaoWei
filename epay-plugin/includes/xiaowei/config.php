<?php
/*小微商户配置*/
include '../common.php';

return [
    'url' => 'https://v.e0x.cn/', //逸轩小微支付系统API地址 官方接口https://v.e0x.cn/
    'mch_id' => '', //商户id 用户中心获取
    'key' => '', //商户key 用户中心获取
    'notify' => ($_SERVER['SERVER_PORT'] == '443' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . '/xiaowei_notify.php', //回调地址
];
?>