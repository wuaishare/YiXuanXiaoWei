package com.example.config;

/**
 * @author ediclot
 * @author prayer
 * @description: 小微支付配置
 * @date 2019/9/8 11:04
 */
public class e0xpayConfig {
    //扫码支付api native
    public static final String NATIVE_PAY_API = "https://v.e0x.cn/pay/order";
    // JSAPI支付api
    public static final String JSAPI_PAY_API = "https://v.e0x.cn/pay/order";
    //退款接口
    public static final String REFUND_API = "https://v.e0x.cn/pay/refund";
    //查询订单接口
    public static final String QUERY_API = "https://v.e0x.cn/pay/query";
    //获取openid
    public static final String OPENID_API = "https://v.e0x.cn/pay/openid";
    //e0x下的app密钥
    public static final String APP_SECRET = "";
    //e0x下的5位商户号ID
    public static final String MCH_ID = "105xx";
    //签约微信分配的小微商户号
    public static final String SUB_MCH_ID = "15660207xx";
    //支付结果通知地址
    public static final String NOTIFY_URL = "http://www.baidu.com";
    //获取openid的回调接口
    public static final String CALL_BACK = "http://localhost/optionCallBack";

    public enum PayType {
        XIAOWEI("微信小微支付"),
        ALIPAYSH("支付宝当面付");

        String desc;

        PayType(String desc) {
            this.desc = desc;
        }
    }

    public enum ChannelType {
        NATIVE("Native支付"),
        JSAPI("JSAPI公众号H5支付");

        String desc;

        ChannelType(String desc) {
            this.desc = desc;
        }
    }
}
