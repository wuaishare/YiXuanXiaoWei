# 易支付对接逸轩小微支付的插件

#### 介绍
逸轩小微支付系统是个人免执照申请微信和支付宝官方接口并快速接入网站使用的一个支付平台，只负责信息流，不接管资金流，资金直接由微信支付和支付宝官方进行结算，微信T1结算至个人银行卡，支付宝D0即时进入账户余额，无任何风险。  
逸轩小微官网 [https://v.e0x.cn/](https://v.e0x.cn/)

#### 优势对比
1. 易支付平台单笔费率3%，逸轩小微费率0.38%，比易支付低8倍多。
2. 易支付平台由平台管理员结算，逸轩小微由微信和支付宝官方结算，无任何跑路风险！
3. 易支付平台支付收款信息显示别人的，逸轩小微支付收款信息显示的信息是自己的。
4. 易支付不支持在线退款，逸轩小微支持原路退款！

#### demo汇总

1. php-demo 下载地址：[点击下载](https://gitee.com/wuaishare/YiXuanXiaoWei/attach_files/316275/download)
2. c-demo 下载地址：[点击下载](https://gitee.com/wuaishare/YiXuanXiaoWei/attach_files/316279/download)
3. java-demo 下载地址：[点击下载](https://gitee.com/wuaishare/YiXuanXiaoWei/attach_files/316285/download)

#### 插件汇总

1. epay-plugin 下载地址：[下载地址](https://gitee.com/wuaishare/YiXuanXiaoWei/attach_files/316280/download)

#### 参与贡献

1. 逸轩 qq740749820 （逸轩小微支付系统-版权所有人）
2. Mr.c qq515073564 （c-demo、java-demo开发者）
3. 青创网络 qq723683150（java-demo修复完善）
4. 琳飞 qq2453095588（php-demo开发者） 

**参与贡献、提供各种插件/demo的朋友，都会视情况奖励一定时长的小微平台VIP套餐，联系客服qq1601695161。**