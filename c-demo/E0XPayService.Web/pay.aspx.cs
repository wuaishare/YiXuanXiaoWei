﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PayService.Web
{
    public partial class pay : System.Web.UI.Page
    {
        public string Reserved
        {
            set { ViewState["Reserved"] = value; }
            get { return ViewState["Reserved"] == null ? "" : ViewState["Reserved"].ToString(); }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString != null && Request.QueryString.Count > 0)
            {
                Dictionary<string, string> dict = new Dictionary<string, string>();
                foreach (string key in Request.QueryString.AllKeys)
                {
                    dict.Add(key, Request.QueryString[key]);
                }
                var v = string.Join(";", dict.Select(s => s.Key + "|" + s.Value));
                Reserved = v;
            }


            if (!IsPostBack)
            {

            }
        }
    }
}