﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pay.aspx.cs" Inherits="PayService.Web.pay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="format-detection" content="email=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />
    <title>商户收款</title>
    <link rel="stylesheet" href="css/common.css?v=3" />
    <style id="__WXWORK_INNER_SCROLLBAR_CSS">
        ::-webkit-scrollbar {
            width: 12px !important;
            height: 12px !important;
        }

        ::-webkit-scrollbar-track:vertical {
        }

        ::-webkit-scrollbar-thumb:vertical {
            background-color: rgba(136, 141, 152, 0.5) !important;
            border-radius: 10px !important;
            background-clip: content-box !important;
            border: 2px solid transparent !important;
        }

        ::-webkit-scrollbar-track:horizontal {
        }

        ::-webkit-scrollbar-thumb:horizontal {
            background-color: rgba(136, 141, 152, 0.5) !important;
            border-radius: 10px !important;
            background-clip: content-box !important;
            border: 2px solid transparent !important;
        }

        ::-webkit-resizer {
            display: none !important;
        }
    </style>
</head>
<body>
        <div class="layout-flex">
            <!-- content start -->
            <div class="content">
                <div class="logo">
                    <img src="http://v53.dededemo.com/plus/img/pay-logo.png">
                </div>
                <div class="amount_title"><em>向</em><span><%=E0XConfig._companyName%></span><em>付款</em></div>
                <div class="set_amount">
                    <div class="amount_hd">消费总额</div>
                    <div class="amount_bd">
                        <i class="i_money">¥</i>
                        <span class="input_simu" id="amount"></span>
                        <!-- 模拟input -->
                        <em class="line_simu" id="line"></em>
                        <!-- 模拟闪烁的光标 -->
                        <div class="clearBtn none" id="clearBtn" style="touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><b class="btn_clear"></b></div>
                        <!-- 清除按钮 -->
                    </div>
                </div>
            </div>
            <!-- content end -->

            <!-- 键盘 -->
            <div class="keyboard">
                <table class="key_table" id="keyboard" style="touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);">
                    <tbody>
                        <tr>
                            <td class="key border b_rgt_btm" data-value="1">1</td>
                            <td class="key border b_rgt_btm" data-value="2">2</td>
                            <td class="key border b_rgt_btm" data-value="3">3</td>
                            <td class="key border b_btm clear delete" data-value="delete"></td>
                        </tr>
                        <tr>
                            <td class="key border b_rgt_btm" data-value="4">4</td>
                            <td class="key border b_rgt_btm" data-value="5">5</td>
                            <td class="key border b_rgt_btm" data-value="6">6</td>
                            <td class="pay_btn disable" rowspan="3" id="payBtn" style="touch-action: pan-y; -webkit-user-select: none; -webkit-user-drag: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><em>付</em>款</td>
                        </tr>
                        <tr>
                            <td class="key border b_rgt_btm" data-value="7">7</td>
                            <td class="key border b_rgt_btm" data-value="8">8</td>
                            <td class="key border b_rgt_btm" data-value="9">9</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="key border b_rgt" data-value="0">0</td>
                            <td class="key border b_rgt" data-value="dot">.</td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
        <div class="circle-box none">
            <div class="circle_animate">
                <div class="circle"></div>
                <p></p>
            </div>
        </div>
        <div class="pop_wrapper none">
            <div class="pop_outer">
                <div class="pop_cont">
                    <div class="pop_tip"></div>
                    <p class="border b_top"><span class="pop_wbtn"></span></p>
                </div>
            </div>
        </div>
        <script src="https://cdn.bootcss.com/zepto/1.2.0/zepto.min.js"></script>
        <script src="js/hammer.js"></script>
        <script src="js/common.js"></script>
        <script type="text/javascript">
            //insert
            function keypress(e) {
                e.preventDefault();
                var target = e.target;
                var value = target.getAttribute('data-value');
                var dot = valueCur.match(/\.\d{2,}$/);
                if (!value || (value !== 'delete' && dot)) {
                    return;
                }
                switch (value) {
                    case '0':
                        valueCur = valueCur === '0' ? valueCur : valueCur + value;
                        break;
                    case 'dot':
                        valueCur = valueCur === '' ? valueCur : valueCur.indexOf('.') > -1 ? valueCur : valueCur + '.';
                        break;
                    case 'delete':
                        valueCur = valueCur.slice(0, valueCur.length - 1);
                        break;
                    default:
                        valueCur = valueCur === '0' ? value : valueCur + value;
                }
                format();
            }

            //format
            function format() {
                var arr = valueCur.split('.');
                var right = arr.length === 2 ? '.' + arr[1] : '';
                var num = arr[0];
                var left = '';
                while (num.length > 3) {
                    left = ',' + num.slice(-3) + left;
                    num = num.slice(0, num.length - 3);
                }
                left = num + left;
                valueFormat = left + right;
                valueFinal = valueCur === '' ? 0 : parseFloat(valueCur);
                check();
            }

            //check
            function check() {
                amount.innerHTML = valueFormat;
                if (valueFormat.length > 0) {
                    clearBtn.classList.remove('none');
                } else {
                    clearBtn.classList.add('none');
                }
                if (valueFinal === 0 || valueCur.match(/\.$/)) {
                    payBtn.classList.add('disable');
                } else {
                    payBtn.classList.remove('disable');
                }
            }

            //clear
            function clearFun() {
                valueCur = '';
                valueFormat = '';
                valueFinal = 0;
                amount.innerHTML = '';
                clearBtn.classList.add('none');
                payBtn.classList.add('disable');
            }

            //submit
            function submitFun() {
                if (!submitAble || payBtn.classList.contains('disable')) { return; }
                if (valueFinal === 0) { tips.show('请输入金额！'); return; }

                if(valueFinal > 1000){ tips.show('支付金额不能大于1k'); return; }

                submitAble = false;
                loading.show();
                callpay();
                return;
            }

            var keyboard = getId('keyboard');
            var amount = getId('amount');
            var clearBtn = getId('clearBtn');
            var payBtn = getId('payBtn');
            var remarkBtn = getId('remarkBtn');
            var remarkPop = getId('remarkPop');
            var remarkInput = getId('remarkInput');
            var remarkCancel = getId('remarkCancel');
            var remarkConfirm = getId('remarkConfirm');
            var valueCur = '';
            var valueFormat = '';
            var valueFinal = 0;
            var submitAble = true;

            new Hammer(keyboard).on('tap', keypress);
            new Hammer(payBtn).on('tap', submitFun);
            new Hammer(clearBtn).on('tap', clearFun);
            remarkInput.value = '';



            //调用微信JS api 支付 
            function callpay() {
                var amount = parseFloat(valueFinal) * 100;
                var reserved = '<% =Reserved %>';
                $.ajax({
                    url: '/Interface.ashx',
                    type: 'POST',
                    data: { 'amount': amount, 'Reserved': reserved },
                    dataType: 'json',
                    timeout: 500000,
                    async: false,
                    //contentType: 'application/json;charset=utf-8',  
                    success: function (response) {
                        if (response.success) {
                            var codeurl = response.codeurl;
                            window.location.href = codeurl;
                        }
                    },
                    error: function (err) {
                        alert(err || "支付失败");
                        return;
                    }
                });

            }
        </script>
</body>
</html>
