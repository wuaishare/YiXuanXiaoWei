﻿using Newtonsoft.Json.Linq;
using E0XPayService.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace E0XPayService.Web
{
    /// <summary>
    /// Interface 的摘要说明
    /// </summary>
    public class Interface : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            try
            {
                var amountStr = context.Request.Form["amount"];
                var attach = context.Request.Form["Reserved"];

                var amount = Convert.ToInt32(amountStr);

                var response = E0XUtils.WxchatPayJSPay(amount, attach);
                if (response.IsSuccess)
                {
                    context.Response.Write(response.Content);
                }
                else
                {
                    context.Response.Write("<script>alert('错误代码：" + response.ErrorCode + ",错误信息：" + response.ErrorMsg + "')</script>");
                }
            }
            catch (Exception ex)
            {
                var json = new JObject();
                json["error_info"] = ex.Message;
                context.Response.Write(json);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}