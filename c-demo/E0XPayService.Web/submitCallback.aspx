﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="submitCallback.aspx.cs" Inherits="E0XPayService.Web.submitCallback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport" />
    <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="black" name="apple-mobile-web-app-status-bar-style" />
    <meta content="telephone=no" name="format-detection" />
    <meta content="email=no" name="format-detection" />
    <link href="../css/wepayuidetail.min.css" rel="stylesheet" />
    <link href="../css/indexdetail.css" rel="stylesheet" />
    <title>支付详情</title>
    <script type="text/javascript">
        function Close() {
            WeixinJSBridge.invoke('closeWindow', {}, function (res) {

                //alert(res.err_msg);

            });
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="weui-wepay-details">
            <div class="weui-wepay-details__hd">
                <div class="weui-wepay-details__state">
                    <h2 class="weui-wepay-details__title"><i class="weui-wepay-details__icon weui-icon-success"></i>已付款</h2>
                    <%--<p class="weui-wepay-details__desc">￥<%=Amount %> </p>--%>
                </div>
            </div>
            <div class="weui-wepay-details__ft">
                <a href="javascript:;" class="weui-btn weui-btn_primary" onclick="Close()">确定</a>
            </div>
        </div>
        <div class="weui-wepay-logos weui-wepay-logos_ft">
            <img src="https://act.weixin.qq.com/static/cdn/img/wepayui/0.1.1/wepay_logo_default_gray.svg" alt="" height="16">
        </div>
    </form>
</body>
</html>

