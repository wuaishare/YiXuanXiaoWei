﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace E0XPayService.Utils
{
    public class JSONHelper
    {
        public static string Serialize<T>(T obj, Formatting format = Formatting.None)
        {
            string ignored = JsonConvert.SerializeObject(obj,
            format, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore});
            return ignored;
        }
        public static T Deserialize<T>(string json)
        {
            using (MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
                T obj = (T)serializer.ReadObject(ms);
                return obj;
            }
        }
    }
}