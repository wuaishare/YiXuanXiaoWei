﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace E0XPayService.Utils
{
    public class E0XConfig
    {
        public readonly static string _apiurl, _mchId, _input_charset, _apiKey
            , _notify_url, _callback_url, _body, _companyName;
        static E0XConfig()
        {
            _apiurl = ConfigurationManager.AppSettings["exapiurl"];
            _mchId = ConfigurationManager.AppSettings["exmchId"];
            _apiKey = ConfigurationManager.AppSettings["exapiKey"];
            _input_charset = ConfigurationManager.AppSettings["exinput_charset"];
            _notify_url = ConfigurationManager.AppSettings["exnotify_url"];
            _callback_url = ConfigurationManager.AppSettings["excallback_url"];
            _body = ConfigurationManager.AppSettings["exbody"];
            _companyName = ConfigurationManager.AppSettings["excompanyName"];

        }
    }
}