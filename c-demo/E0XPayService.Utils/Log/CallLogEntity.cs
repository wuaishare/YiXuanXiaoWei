﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E0XPayService.Utils
{
    public class CallLog
    {
        public CallLog()
        {
            OperationStart = DateTime.Now;
        }

        internal const string LogDataTimeFormat = "yyyyMMdd HH:mm:ss.fff";

        public string OperationName { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperationStart { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperationEnd { get; set; }

        /// <summary>
        /// 请求参数
        /// </summary>
        public string RequestDetail { get; set; }

        /// <summary>
        /// 返回明细
        /// </summary>
        public string ResponseDetail { get; set; }


        public string ResultInfo { get; set; }

        /// <summary>
        /// 其他内容
        /// </summary>
        public string OtherInfo { get; set; }

        public override string ToString()
        {
            return Environment.NewLine + string.Format(
@"|| 操作名称：{0} || 开始时间：{1} || 完成时间：{2}
    || Request明细：{3}
    || Response明细：{4}
    || Result明细：{5}
    {6}
",OperationName, OperationStart.ToString(LogDataTimeFormat), OperationEnd.ToString(LogDataTimeFormat), RequestDetail, ResponseDetail, ResultInfo
, string.IsNullOrWhiteSpace(OtherInfo) ? "" : "|| 其他内容：" + OtherInfo);
        }
    }

    public class CallErrorLog
    {
        public CallErrorLog(string invokeDetail, Exception exception)
            : this(invokeDetail, exception.Message, exception.ToString())
        { }

        public CallErrorLog(string invokeDetail, string message, string details = null)
        {
            InvokeDetail = invokeDetail;
            ErrorMessage = message;
            ErrorDetails = details;
            ExceptionTime = DateTime.Now;
        }

        /// <summary>
        /// 异常时间
        /// </summary>
        public DateTime ExceptionTime { get; set; }

        /// <summary>
        /// 操作明细
        /// </summary>
        public string InvokeDetail { get; set; }
        /// <summary>
        /// 异常消息
        /// </summary>
        public string ErrorMessage { get; set; }
        /// <summary>
        /// 异常明细
        /// </summary>
        public string ErrorDetails { get; set; }

        public override string ToString()
        {
            return Environment.NewLine + string.Format(@"|| 异常时间：{0}
    || 异常消息：{1}
    || 异常明细：{2}
    || 请求明细：{3}
", ExceptionTime.ToString(CallLog.LogDataTimeFormat), ErrorMessage, ErrorDetails, InvokeDetail);
        }
    }
}