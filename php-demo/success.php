<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/13 13:00:52
 * E-mail: admin@eyabc.cn
 */

/**
 * 交易完成
 */
require 'lib/common.php';

// 订单号
$order = input('order');

if (!orderIsPay($order, $result)) {
    die('订单未支付');
}

require 'view/success.php';