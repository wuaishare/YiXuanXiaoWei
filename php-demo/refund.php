<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/13 13:36:22
 * E-mail: admin@eyabc.cn
 */

/**
 * 订单退款
 */
require 'lib/common.php';

// 订单号
$order = input('order');

// 订单退款
list($result, $err) = $sdk->refund($order);

if (!$result) {
    $message = ['status' => 0, 'info' => $err];
} else {
    $message = ['status' => 1, 'info' => 'success'];
}

echo json_encode($message);