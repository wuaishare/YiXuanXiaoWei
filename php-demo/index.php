<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/22 22:05:08
 * E-mail: admin@eyabc.cn
 */
require 'lib/common.php';

if (!empty($_POST)) {
    // 支付类型
    $type = input('type', 'wxpay');
    // 订单号
    $out_trade_no = input('out_trade_no', date('YmdHis') . mt_rand(1000, 9999));
    // 商品标题
    $subject = input('subject', '测试支付');
    // 支付金额
    $total_fee = (float)input('total_fee', '0.01') * 100;

    // 获取Openid回调地址 这里传参只是测试使用，正式使用时只需要传订单号即可
    $callback = $domain . 'openid.php?' . http_build_query(['out_trade_no' => $out_trade_no, 'subject' => $subject, 'total_fee' => $total_fee]);
    // 异步回调地址
    $notify = $xwConfig['notify'];

    switch ($type) {
        case 'alipay':
            // 支付宝当面付通道

            // 创建订单
            list($result, $err) = $sdk->order($out_trade_no, $total_fee, 'ALIPAYSH', 'DMF', $notify, $subject, null, $xwConfig['ali_sub_mch_id']);
            if (!$result) {
                die('支付宝下单失败, Error: ' . $err);
            }

            // 二维码地址
            $url = $result['data']['code_url'];
            require 'view/alipay.php';
            break;
        default:

            if (isMobile()) {
                // 为微信JSAPI通道获取openid
                $url = $sdk->getOpenidUrl($callback, $xwConfig['sub_mch_id']);
            } else {
                // 微信扫码通道

                // 创建订单
                list($result, $err) = $sdk->order($out_trade_no, $total_fee, 'XIAOWEI', 'NATIVE', $notify, $subject, null, $xwConfig['sub_mch_id']);
                if (!$result) {
                    die('微信下单失败, Error: ' . $err);
                }

                // 二维码地址
                $url = $result['data']['code_url'];
            }
            require isMobile() && isWeiXin() ? 'view/checkout.php' : 'view/weixin.php';
            break;
    }

    die();
}

require 'view/index.php';