<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/12 12:56:38
 * E-mail: admin@eyabc.cn
 */

/**
 * 异步回调处理
 */
require 'lib/common.php';

// 签名验证成功
if ($sdk->notify()) {

    // 业务代码 ...

    // 处理完成返回参数
    echo '{"status":0,"message":"OK"}';
} else {
    echo '{"status":1,"message":"OK"}';
}