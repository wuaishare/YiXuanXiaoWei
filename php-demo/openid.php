<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/12 12:16:22
 * E-mail: admin@eyabc.cn
 */

/**
 * 微信JSAPI下单
 */
require 'lib/common.php';

/** 自定义回调参数 **/
// 订单号
$out_trade_no = input('out_trade_no');
// 商品标题
$subject = input('subject');
// 支付金额
$total_fee = input('total_fee');

/** 小微回调参数 **/
// 微信子商户
$sub_mch_id = input('sub_mch_id') ?: $xwConfig['sub_mch_id'];
// 用户标识
$openid = input('openid');

// 异步回调地址
$notify = $xwConfig['notify'];

// 创建订单
list($result, $err) = $sdk->order($out_trade_no, $total_fee, 'XIAOWEI', 'JSAPI', $notify, $subject, $openid, $sub_mch_id);
if (!$result) {
    die('微信下单失败, Error: ' . $err);
}

require 'view/checkout.php';