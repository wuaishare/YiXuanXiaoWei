<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/23 23:10:28
 * E-mail: admin@eyabc.cn
 */

if (!function_exists('url')) {
    /**
     * 获取程序URL根目录地址
     * @access public
     * @return string
     */
    function url()
    {
        // 运行目录
        $root = $_SERVER['DOCUMENT_ROOT'];
        // 源码运行目录
        $dir = dirname(__DIR__);
        // 取出URI
        $uri = substr($dir, strlen($root));
        // 替换分隔符
        $uri = str_replace('\\', '/', $uri);
        // 删除左边的分隔符
        $uri = rtrim($uri, '/') . '/';
        // 拼接URL
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $uri;

        return $url;
    }
}
if (!function_exists('isMobile')) {
    /**
     * 检测手机打开
     * @return bool
     */
    function isMobile()
    {
        static $is_mobile;
        if (isset($is_mobile)) {
            return $is_mobile;
        }
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            $is_mobile = false;
        } elseif
        (strpos($_SERVER['HTTP_USER_AGENT'],
                'Mobile') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'Android') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'Silk/') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'Kindle') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'BlackBerry') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'Opera Mini') !== false || strpos($_SERVER['HTTP_USER_AGENT'],
                'Opera Mobi') !== false
        ) {
            $is_mobile = true;
        } else {
            $is_mobile = false;
        }
        return $is_mobile;
    }
}

if (!function_exists('isWeiXin')) {
    /**
     * 检测微信内置浏览器打开
     * @return bool
     */
    function isWeiXin()
    {
        $ua = $_SERVER['HTTP_USER_AGENT'];
        return !(strpos($ua, 'MicroMessenger') === false);
    }
}

if (!function_exists('input')) {
    /**
     * 获取请求参数值
     * @access public
     * @param string $name
     * @param mixed $default
     * @return mixed
     */
    function input($name, $default = null)
    {
        return isset($_REQUEST[$name]) ? addslashes(trim($_REQUEST[$name])) : $default;
    }
}

if (!function_exists('orderIsPay')) {
    /**
     * 查询订单是否付款
     * @param string $order 商户订单号
     * @param null $result 返回信息
     * @return bool
     */
    function orderIsPay($order, &$result = null)
    {
        global $sdk;

        // 查询订单交易状态 正式使用时为保证效率，直接查询数据库即可
        list($result, $err) = $sdk->query($order);

        // 返回订单状态
        return $result && isset($result['data']['status']) && $result['data']['status'] == 1;
    }
}