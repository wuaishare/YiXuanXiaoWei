<?php

/**
 * Created by PhpStorm.
 * User: 琳飞
 * Created time 2019/12/21 21:21:28
 * E-mail: admin@eyabc.cn
 */

require_once 'function.php';
require_once 'sdk.php';

// 当前站点地址
$domain = url();

// 配置
$xwConfig = [
    // 商户ID
    'mch_id' => '你的商户id',
    // 商户秘钥
    'key' => '你的商户key',
    // 微信子商户
    'sub_mch_id' => null,
    // 支付宝子商户
    'ali_sub_mch_id' => null,

    // 异步回调地址
    'notify' => $domain . 'notify.php',
    // 同步回调地址
    'return' => $domain . 'success.php',
];

// 实例化SDK类
$sdk = new Sdk($xwConfig['mch_id'], $xwConfig['key']);