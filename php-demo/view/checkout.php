<!DOCTYPE html>
<html>

<head>
    <title>微信安全支付</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no">
    <style type="text/css">
        body {
            padding: 0;
            margin: 0;
            background-color: #eeeeee;
            font-family: '黑体';
        }

        .pay-main {
            background-color: #4cb131;
            padding-top: 20px;
            padding-left: 20px;
            padding-bottom: 20px;
        }

        .pay-main img {
            margin: 0 auto;
            display: block;
        }

        .pay-main .lines {
            margin: 0 auto;
            text-align: center;
            color: #cae8c2;
            font-size: 12pt;
            margin-top: 10px;
        }

        .tips .img {
            margin: 20px;
        }

        .tips .img img {
            width: 20px;
        }

        .tips span {
            vertical-align: top;
            color: #ababab;
            line-height: 18px;
            padding-left: 10px;
            padding-top: 0px;
        }

        .action {
            background: #4cb131;
            padding: 10px 0;
            color: #ffffff;
            text-align: center;
            font-size: 14pt;
            border-radius: 10px 10px;
            margin: 15px;
        }

        .action:focus {
            background: #4cb131;
        }

        .action.disabled {
            background-color: #aeaeae;
        }

        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            text-align: center;
            padding-bottom: 20px;
            font-size: 10pt;
            color: #aeaeae;
        }

        .footer .ct-if {
            margin-top: 6px;
            font-size: 8pt;
        }
    </style>
</head>

<body>
<div class="conainer">
    <div class="pay-main">
        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALQAAAC0CAMAAAAKE/YAAAAAk1BMVEUAAAD////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////ROyVeAAAAMXRSTlMA5szk4tispg5mwclzKt/cvLKAVwXRR8ZNMhgcZCYMn5kVQDmFkQi3biADUoxeqnphkvTIVAAABgNJREFUeNrs1G1zojAUhmHDc5DyUmxYY9GtiCxKbWt3//+v20Hr6LTqbuU04PTcfMuHcE3mJD1J+lSTyHMs5UUTHrMfOBYLfAZyljuWy7PGaM+xntd4NpwW8hveQaeVmt3GyGml6NomuvlUOy0l6BMJWtBnEvT3Rvf+v/b/J2hBC1rQgt4laEELWtCCFrSg6wQtaEELWtCCFnSdoAUtaEELWtCCrvu26On6YbS8LvQ8MgQK0ytCDwtNABRiPd4s5FeALmP1FtxyUq+MdcfRYw21bzcjqe4wOvMN1GEg7/aN3VX0fEbqfTDJz17dqJvo1EB9DBQ+ZN147D82ncXqeKC7opPoLHKhToZY/+oeOjWkzgbS6bJL6OGoIqh/BQr+3HQFffOs9+SzAW5ePLWPfpqWzjvyeTaZl6PTvXr0qkXBhx7ejtLp8OiDkYQEqM8FcvXv8fJg/9WPmSEAccmH9gcAuUG+PpTPi6RyAaiLAmhggqqvq7vQxITtRtB86BwK2yh2TXh/b9wBETapy8O+3ULFh9ZK4WiKM+aT7m90A9oisfm+JPT50K8njJ1Ge9bQr6wX0U7w+NALa+gZH7q0hs750Ik19IIP/WgN/cKHXltDl3zowho64UOvrKEjPnRmDe3zoXuW0MCaEU0XCMLwAvQzI/pve/fanCYQhmH4Wd/lqBwEhAhGi0bjKUn//69rYvvBmu7qyss2nek9/dy5Qh5QHCfEwjSnBErHGL1kRCem5rQBgCY1RUeMaMdw1K6PU75riG4Z0QMz9NnVNjNDF4zolMzmfJbJsEmCEe0ZoNc+fstf345OONFPGnR0vltn3eBTzdo533ukRjuc6JkGDTT5yD2VlVBUZu6pUd4AavSAE73SoY1TminkRLeW0K98aP3r+AqGrdToIyvasYOesaIr9T5KGFaq0StWtKtGj2DYSI2uWdFPVtCyYEWXarQLw1zNawsrupU20CEnWv+Omu8y7TGjh2q0D6N8NfqNE63/iHrFdpleMqO3avQMRs2UaNkyo6dqdAajMiU62TOjN7ES7XJdPCowozHgunxoPpxmRy/U6AgGRWp0zo7ONWci03lYs6NrqUQvWH5j5OzZ0Zgo0Q4McpRoF/xoTzNqlkkHPaBf1OgZw6SF3PWArkmoSnFzqXpke3a0/j6xwY01QpkHdrR+1DluLNf/H/zoB9H9ldxVT7ruBe3Hmn10XscEBmiefWTG7/Aue+sJ/UBC1bDr/Q9FPaHnieY06ngaOjBAc+1j2PFGc9Eb+kCaQ93pQMuyN/TG0RzqTgd6sjFHc3yZaWTwcZjmpoUfPZWaU8mHNt/RrGPZIxoV3X0uLTQHupr3id5Kcec71JnB1zyY0fWENAOJoCxyNOa47hWNIwl1Q1856KHBVwjZ0VGsU6e+wpwKTXLZMxoeCa3a3Cyqfd/opTRW680kD+gbXbgkdA2jT4saCi26Qu9olFJoc2YX1zpHb5YvFtD7MQl9bnN2r+IKfVT5FtBYJSSutG5+kdfimlkGsIHGgsTVFrnv5wtxNUpbO+hoQIIreYAdNEaSy0zPhS10GxIPmZwIttDIYxazkE+wh8ZaMpiJnlub6JphICSSEjbRKBPRmS2zwi66yDoPhKo57KIxH0tBnRY9nMI2GruUuqgpeYF9NJYJCWIYtFU0th1ORrko8FfQCGK61+y1+EtoZHeqqdrBGM1R+ADMn+5Sy/QR5mie3tUYJeZqSqdgRJurN6ZqErJ6BCvaXI18Qgbkd7O3g1V0GEynQXihLm9XkyD5rYVVdHj6vT5eqqfP8tbDTElWwC46+MMz/3IAu2+S6KZpVHkBy+gpTk0H570BwNahG8xURbg7jxM98OYAoldJV6dx9HF/I7Z5nBp/TN3fTkhDJiHThwIdqllOxMufZvcaK5ZN7/+cY4NuBRyXvPO8RwCbPJT052XIdIUv+KDS0RxAG1Sf2EQkw++br/lI2DD4YPuzCzaRHG5bfNmH74ZBDQCHc/YHuf7ijzk+LgEUh3FM9PMoD4IN/pF+/nEmiseHAv9S/vfsLdrjf//rqR+ooHrLLTW8JQAAAABJRU5ErkJggg=="/>
        <div class="lines"><span>微信安全支付</span>
        </div>
    </div>
    <div class="tips">
        <div class="img">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwOS8yNS8xNNgt9LkAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAABWUlEQVRIie3XP1KDQBTH8S8OfXIEaxsdTpALLMkNxGZrvUAaL5CaRryBwgHkBEysbPUG8QJisW9HBvmbAJkx/jo2JJ995L1AnDzPOUZcgDAMc2CqHThaa8eVg1xrfTaFGobhF8AkWFX+4b8Pu+2n9IuKvTmQAu9AkPjZruq8MSreAJfAUjZQmUFhFXsRcF1Y2o4Oq9gLSuhz4mfBqLCgD4WlV6AWHQRWsXeF+V5tPoBFXVMNAguaAjNZ+gRWbWgrLB9c99ociAoomEprG6oTbKtRsfckSBlNMWNjc9MVbYT5uYRL2UCxejurNneJn0VdUWj+5boVYCZIqmJvhenW4tg8Jn62+f325tRWLBUsMA2DbOCFHrO6Fyz4FjjHzGU5rbO6Nyz4DlP5W2G506weBFs88bML4J4es9qUXrfFxM/WwPoQ0Ob0nkBOD7bN5dgn/AniADjH+tP2DUWacPfNwbEbAAAAAElFTkSuQmCC"/>
            <span>已开启支付安全</span>
        </div>
    </div>
    <div id="action" class="action" onclick="onBridgeReady();">确认支付</div>
    <div class="footer">
        <div>支付安全由中国人民财产保险股份有限公司承保</div>
        <div class="ct-if"></div>
    </div>
</div>

<script src="https://g.onlinfei.com/xiaowei/js/??jquery.min.js,qcloud_util.js?v1"></script>

<?php if ($result['data']['pay_info']) { ?>
    <script src="https://g.onlinfei.com/layer/layer.js?v1"></script>
    <script>
        function onBridgeReady() {
            WeixinJSBridge.invoke('getBrandWCPayRequest', <?php echo $result['data']['pay_info']; ?> , function (res) {
                if (res.err_msg === "get_brand_wcpay_request:ok") {
                    var loadMsg = layer.msg('付款成功, 正在验证中...', {time: 0});
                    // 检查是否支付完成
                    var r = window.setInterval(function () {
                        $.ajax({
                            type: 'POST',
                            url: '<?php echo $domain . 'query.php'; ?>',
                            data: 'order=<?php echo $out_trade_no; ?>',
                            dataType: 'json',
                            success: function (res) {
                                if (res.status === 1) {
                                    layer.msg('验证成功, 正在返回', {time: 0});
                                    window.location.href = res.url;
                                    window.clearInterval(r);
                                }
                            }
                        });
                    }, 2000);
                }
            });
        }

        function close() {
            WeixinJSBridge.call('closeWindow');
        }

        $(document).ready(function () {
            document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() {
                // 通过下面这个API隐藏右上角按钮
                WeixinJSBridge.call('hideOptionMenu');
            });
        });
        $(document).ready(function () {
            $('body').on('touchmove', function (event) {
                event.preventDefault();
            });
        });

        if (typeof WeixinJSBridge == "undefined") {
            if (document.addEventListener) {
                document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
            } else if (document.attachEvent) {
                document.attachEvent('WeixinJSBridgeReady', onBridgeReady);
                document.attachEvent('onWeixinJSBridgeReady', onBridgeReady);
            }
        } else {
            onBridgeReady();
        }
    </script>

<?php }else{ ?>

    <script src="https://g.onlinfei.com/layer/mobile/layer.js?v1"></script>
    <script>
        $(function () {
            layer.open({
                type: 2,
                content: '正在提交支付'
            });
            window.location.href = "<?php echo $url; ?>";
        });
    </script>

<?php } ?>
</body>

</html>