<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>小微支付 - 在线测试</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css?v1" rel="stylesheet">
    <link rel="stylesheet" href="https://css.letvcdn.com/lc04_yinyue/201612/19/20/00/bootstrap.min.css?v1">
    <script src="https://cdn.bootcss.com/jquery/1.11.3/jquery.min.js?v1"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js?v1"></script>
</head>
<body>
<div class="container" style="padding-top:70px;">
    <div class="col-xs-12 col-sm-10 col-lg-8 center-block" style="float: none;">
        <div class="panel panel-primary">
            <div class="panel-body">
                <form action="" method="post"><!-- target="_blank" -->
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-barcode"></span>
                        </span>
                        <input size="30" name="out_trade_no"
                               value="<?php echo date('YmdHis') . mt_rand(1000, 9999) ?>" class="form-control"
                               placeholder="商户订单号">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-shopping-cart"></span>
                        </span>
                        <input size="30" name="subject" value="测试商品" class="form-control" placeholder="商品名称"
                               required="required">
                    </div>
                    <br>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-yen"></span>
                        </span>
                        <input size="30" name="total_fee" value="0.01" class="form-control" placeholder="付款金额"
                               required="required">
                    </div>
                    <br>
                    <div>
                        <div class="btn-group btn-group-justified" role="group" aria-label="...">
                            <div class="btn-group" role="group">
                                <button type="radio" name="type" value="alipay" class="btn btn-primary">支付宝</button>
                            </div>
                            <div class="btn-group" role="group">
                                <button type="radio" name="type" value="wxpay" class="btn btn-info">微信</button>
                            </div>
                        </div>
                        <p style="text-align:center">
                            <br>&copy; 技术支持 <a href="/">小微支付</a>!</p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>