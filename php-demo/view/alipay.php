<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Language" content="zh-cn">
    <meta name="renderer" content="webkit">
    <title>支付宝扫码支付</title>
    <link href="https://g.onlinfei.com/xiaowei/css/style.css?v3" rel="stylesheet" media="screen">
</head>
<body>
<div class="body">
    <h1 class="mod-title">
        <span class="ico-alipay"></span><span class="text">支付宝支付</span>
    </h1>
    <div class="mod-ct">
        <div class="order">
        </div>
        <div class="amount">￥<?php echo $total_fee / 100; ?></div>
        <div class="qr-image" id="qrcode">
        </div>
        <!-- 唤起支付宝App -->
        <div class="open_app" style="display: none;">
            <a href="alipays://platformapi/startapp?appId=20000067&url=<?php echo $url; ?>">打开支付宝APP</a>
        </div>

        <div class="detail" id="orderDetail">
            <dl class="detail-ct" style="display: none;">
                <dt>购买物品</dt>
                <dd id="productName"><?php echo $subject; ?></dd>
                <dt>商户订单号</dt>
                <dd id="billId"><?php echo $out_trade_no; ?></dd>
                <dt>创建时间</dt>
                <dd id="createTime"><?php echo date('Y-m-d H:i:s'); ?></dd>
            </dl>
            <a href="javascript:void(0)" class="arrow"><i class="ico-arrow-alipay"></i></a>
        </div>
        <div class="tip">
            <span class="dec dec-left"></span>
            <span class="dec dec-right"></span>
            <div class="ico-scan-alipay"></div>
            <div class="tip-text">
                <p>请使用支付宝扫一扫</p>
                <p>扫描二维码完成支付</p>
            </div>
        </div>
        <div class="tip-text">
        </div>
    </div>
</div>
<script src="https://g.onlinfei.com/xiaowei/js/??jquery.min.js,qrcode.min.js,qcloud_util.js?v1"></script>
<script>
    $(document).ready(function () {
        var qrcode = new QRCode("qrcode", {
            text: "<?php echo $url; ?>",
            width: 230,
            height: 230,
            colorDark: "#000000",
            colorLight: "#ffffff",
            correctLevel: QRCode.CorrectLevel.H
        });
        var r = window.setInterval(function () {
            $.ajax({
                type: 'POST',
                url: '<?php echo $domain . 'query.php'; ?>',
                data: 'order=<?php echo $out_trade_no; ?>',
                dataType: 'json',
                success: function (res) {
                    if (res.status === 1) {
                        window.location.href = res.url;
                        window.clearInterval(r);
                    }
                }
            });
        }, 2000);

        // 订单详情
        $('#orderDetail .arrow').click(function (event) {
            if ($('#orderDetail').hasClass('detail-open')) {
                $('#orderDetail .detail-ct').slideUp(500, function () {
                    $('#orderDetail').removeClass('detail-open');
                });
            } else {
                $('#orderDetail .detail-ct').slideDown(500, function () {
                    $('#orderDetail').addClass('detail-open');
                });
            }
        });

        var isMobile = function () {
            var ua = navigator.userAgent;
            var ipad = ua.match(/(iPad).*OS\s([\d_]+)/),
                isIphone = !ipad && ua.match(/(iPhone\sOS)\s([\d_]+)/),
                isAndroid = ua.match(/(Android)\s+([\d.]+)/);
            return isIphone || isAndroid;
        };

        if (isMobile()) {
            $('.open_app').css({
                display: 'block'
            });
        }
    });
</script>
</body>
</html>