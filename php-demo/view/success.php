<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>小微支付 - 支付成功</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css?v1" rel="stylesheet">
    <link rel="stylesheet" href="https://css.letvcdn.com/lc04_yinyue/201612/19/20/00/bootstrap.min.css?v1">
    <script src="https://cdn.bootcss.com/jquery/1.11.3/jquery.min.js?v1"></script>
    <script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js?v1"></script>
    <script src="https://g.onlinfei.com/layer/layer.js?v1"></script>
</head>
<body>
<div class="container" style="padding-top:70px;">
    <div class="text-center" style="margin-bottom: 23px;">
        <h4>支付成功</h4>
    </div>
    <div class="col-xs-12 col-sm-10 col-lg-8 center-block" style="float: none;">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-barcode"></span>
                        </span>
                    <input name="out_trade_no"
                           value="<?php echo $order; ?>" class="form-control" readonly="">
                </div>
                <br>
                <div class="input-group">
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-yen"></span>
                        </span>
                    <input name="total_fee" value="<?php echo $result['data']['total_fee'] / 100; ?>"
                           class="form-control">
                </div>
                <br>
                <div>
                    <div class="btn-group btn-group-justified" role="group" aria-label="...">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-primary refund">退款</button>
                        </div>
                    </div>
                    <p style="text-align:center">
                        <br>&copy; 技术支持 <a href="/">小微支付</a>!</p>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('.refund').on('click', function () {
                var loadMsg = layer.load();
                $.post('<?php echo $domain . 'refund.php';?>', {order: '<?php echo $order;?>'}, function (res) {
                    if (res.status === 1) {
                        layer.msg('退款申请成功', {time: 1500, icon: 1});
                    } else {
                        layer.msg(res.info);
                    }
                }, 'json').fail(function () {
                    layer.msg('服务器繁忙, 请稍后再试');
                }).complete(function () {
                    layer.close(loadMsg);
                });
            });
        })
    </script>
</div>
</body>
</html>